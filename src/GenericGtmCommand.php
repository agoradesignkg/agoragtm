<?php

namespace Drupal\agoragtm;

/**
 * Defines the generic GTM command class.
 */
class GenericGtmCommand implements GtmCommandInterface {

  /**
   * The default priority.
   *
   * @var int
   */
  const DEFAULT_PRIORITY = 0;

  /**
   * The data.
   *
   * @var array
   */
  protected $data;

  /**
   * The priority.
   *
   * @var int
   */
  protected $priority;

  /**
   * Constructs a new GenericGtmCommand object.
   *
   * @param array $data
   *   The command data. Defaults to an empty array.
   * @param int $priority
   *   The priority.
   */
  public function __construct(array $data = [], $priority = self::DEFAULT_PRIORITY) {
    $this->data = $data;
    $this->priority = $priority;
  }

  /**
   * {@inheritdoc}
   */
  public function getData() {
    return $this->data;
  }

  /**
   * {@inheritdoc}
   */
  public function getPriority() {
    return $this->priority;
  }

  /**
   * {@inheritdoc}
   */
  public function toArray() {
    return [
      'data' => $this->getData(),
    ];
  }

}
