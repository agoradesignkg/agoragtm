<?php

namespace Drupal\agoragtm;

use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * Default command registry implementation.
 */
class CommandRegistry implements CommandRegistryInterface {

  /**
   * The registered analytics commands.
   *
   * @var \Drupal\agoragtm\GtmCommandInterface[]
   */
  protected $commands;

  /**
   * The private temp store object.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $privateTempStore;

  /**
   * Constructs a new CommandRegistry object.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $private_temp_store_factory
   *   The private temp store factory.
   */
  public function __construct(PrivateTempStoreFactory $private_temp_store_factory) {
    $this->privateTempStore = $private_temp_store_factory->get('agoragtm');
    $this->commands = [];
    $delayed_commands = $this->privateTempStore->get('commands');
    if ($delayed_commands && is_array($delayed_commands)) {
      foreach ($delayed_commands as $command) {
        $this->commands[] = $command;
      }
      $this->privateTempStore->delete('commands');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function addCommand(GtmCommandInterface $command) {
    $this->commands[] = $command;
  }

  /**
   * {@inheritdoc}
   */
  public function addDelayedCommand(GtmCommandInterface $command) {
    $delayed_commands = $this->privateTempStore->get('commands');
    $delayed_commands = $delayed_commands ?: [];
    $delayed_commands[] = $command;
    $this->privateTempStore->set('commands', $delayed_commands);
  }

  /**
   * {@inheritdoc}
   */
  public function getCommands() {
    return $this->commands;
  }

  /**
   * {@inheritdoc}
   */
  public function getDrupalSettingCommands() {
    usort($this->commands, function (GtmCommandInterface $a, GtmCommandInterface $b) {
      return $b->getPriority() - $a->getPriority();
    });

    $commands_array = [];
    foreach ($this->commands as $command) {
      $commands_array[] = $command->toArray();
    }
    return $commands_array;
  }

}
