<?php

namespace Drupal\agoragtm;

/**
 * Defines the command registry interface.
 */
interface CommandRegistryInterface {

  /**
   * Add a command to the registry.
   *
   * @param \Drupal\agoragtm\GtmCommandInterface $command
   *   An analytics command.
   */
  public function addCommand(GtmCommandInterface $command);

  /**
   * Add a delayed command to the registry.
   *
   * Delayed command will be sent on the following page request.
   *
   * @param \Drupal\agoragtm\GtmCommandInterface $command
   *   An analytics command.
   */
  public function addDelayedCommand(GtmCommandInterface $command);

  /**
   * Get all commands registered.
   *
   * @return \Drupal\agoragtm\GtmCommandInterface[]
   *   The array of registered commands.
   */
  public function getCommands();

  /**
   * Format the commands for use in drupalSettings.
   *
   * @return array
   *   An array of commands for use in drupalSettings.
   */
  public function getDrupalSettingCommands();

}
