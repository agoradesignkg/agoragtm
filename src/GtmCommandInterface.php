<?php

namespace Drupal\agoragtm;

/**
 * Defines the GTM command interface.
 */
interface GtmCommandInterface {

  /**
   * Get the data.
   *
   * @return array
   *   The data.
   */
  public function getData();

  /**
   * An integer value for sorting by priority.
   *
   * @return int
   *   The priority value.
   */
  public function getPriority();

  /**
   * Get an array representation of the command.
   *
   * @return array
   *   The tracking command as array.
   */
  public function toArray();

}
