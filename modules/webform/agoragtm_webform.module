<?php

use Drupal\agoragtm\GenericGtmCommand;
use Drupal\Core\Form\FormStateInterface;

/**
 * Gets the configured event name to be uesed form webform submissions.
 *
 * @return string
 *   The GTM event name.
 */
function agoragtm_webform_get_event_name() {
  $event_name = \Drupal::config('agoragtm_webform.settings')->get('event_name');
  if (empty($event_name)) {
    $event_name = 'webform_submission';
  }
  return $event_name;
}

/**
 * Implements hook_form_alter().
 */
function agoragtm_webform_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  if (!agoragtm_is_tracking_enabled()) {
    return;
  }
  if (strpos($form_id, 'webform_submission_') === 0 && !empty($form['#webform_id'])) {
    if (!empty($form['actions']['submit']['#submit'])) {
      $form['actions']['submit']['#submit'][] = 'agoragtm_webform_track_on_form_submit';
    }
    else {
      $form['#submit'][] = 'agoragtm_webform_track_on_form_submit';
    }
  }
}

/**
 * Form submit callback for tracking webform submissions.
 */
function agoragtm_webform_track_on_form_submit($form, FormStateInterface $form_state) {
  /** @var \Drupal\agoragtm\CommandRegistryInterface $command_registry */
  $command_registry = \Drupal::service('agoragtm.command_registry');

  $command_data = [
    'event' => agoragtm_webform_get_event_name(),
    'formId' => $form['#webform_id'],
  ];
  $command = new GenericGtmCommand($command_data);
  $command_registry->addDelayedCommand($command);
}
