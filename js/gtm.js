/**
 * @file
 * Initialize GTM pixel on the page.
 */

(function (drupalSettings) {
  'use strict';

  if (!drupalSettings.agoragtm) {
    return;
  }
  var gtmSettings = drupalSettings.agoragtm;

  /*eslint-disable */
  (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer',gtmSettings.gtm_id);
  /*eslint-enable */

  window.dataLayer = window.dataLayer || [];
  for (var i = 0; i < gtmSettings.commands.length; i++) {
    var command = gtmSettings.commands[i];
    window.dataLayer.push(command.data);
  }

})(drupalSettings);
